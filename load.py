import getopt
import os
import subprocess
import sys
import time

port = 5201
nexthop = 'localhost'
remote_port = 5201
network_load = 'low'
copmute_load = 'low'
if not os.path.exists('logs'):
    os.makedirs('logs')

act_as_client = False
act_as_server = False
act_as_stress = False

# get options
opts, args = getopt.getopt(sys.argv[1:], 'p:h:r:n:c:', ['port=', 'host=', 'remote_port=', 'network_load=', 'compute_load='])
for opt, arg in opts:
    if opt in ['-p', '--port']:
        port = arg
        act_as_server = True
    elif opt in ['-h', '--host']:
        nexthop = arg
        act_as_client = True
    elif opt in ['-r', '--remote_port']:
        remote_port = arg
        act_as_client = True
    elif opt in ['-n', '--network_load']:
        network_load = arg
        act_as_client = True
    elif opt in ['-c', '--compute_load']:
        copmute_load = arg
        act_as_stress = True


def iperf3_server():
    # iperf3 server side
    if act_as_server:
        iperfs_command = "iperf3 -s -p " + port
        iperfs = subprocess.Popen(
            iperfs_command,
            stdout=open('logs/server_stdout', 'a'),
            stderr=open('logs/server_stderr', 'a'),
            shell=True
        )
        return iperfs
    return None

def iperf3_client():
    # iperf3 client side
    if act_as_client:
        iperfc_command = "iperf3 -t -1 -c " + nexthop + " -p " + remote_port

        if network_load == 'low':
            iperfc_command += ' -u -b 10M'
        elif network_load == 'med':
            iperfc_command += ' -u -b 50M'
        elif network_load == 'high':
            iperfc_command += ' -u -b 100M'
        else:
            iperfc_command = ''

        iperfc = subprocess.Popen(
            iperfc_command,
            stdout=open('logs/client_stdout', 'a'),
            stderr=open('logs/client_stderr', 'a'),
            shell=True
        )
        return iperfc
    return None

def stress_ng():
    # stress-ng client side
    if act_as_stress:
        stress_command = 'stress-ng --timeout 0'

        if copmute_load == 'low':
            stress_command += ' --cpu 1 --vm 1 --vm-bytes 1024M'
        elif copmute_load == 'med':
            stress_command += ' --cpu 2 --vm 1 --vm-bytes 2048M'
        elif copmute_load == 'high':
            stress_command += ' --cpu 4 --vm 1 --vm-bytes 6072M'
        else:
            stress_command = ''

        stress = subprocess.Popen(
            stress_command,
            stdout=open('logs/stress_stdout', 'a'),
            stderr=open('logs/stress_stderr', 'a'),
            shell=True
        )
        return stress
    return None
    
iperfs = iperf3_server()
iperfc = iperf3_client()
stress = stress_ng()

while any([proc is not None for proc in [iperfs, iperfc, stress]]):
    if iperfs is None or iperfs.poll() is not None:
        iperfs = iperf3_server()
    
    if iperfc is None or iperfc.poll() is not None:
        iperfc = iperf3_client()
    
    if stress is None or stress.poll() is not None:
        stress = stress_ng()
    
    time.sleep(10)
