FROM debian:bullseye-slim
LABEL author="Abderrahmane Boudi <abr.boudi@gmail.com>"

# install binary and remove cache
RUN apt-get update \
  && apt-get install -y iperf3 python \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY --from=alexeiled/stress-ng /stress-ng /bin/

COPY load.py /

ENTRYPOINT ["python"]
