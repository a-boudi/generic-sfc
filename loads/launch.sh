#!/bin/bash
dir=$1

for service in $dir//services/* ; do 
  kubectl apply -f $service
done

for deployment in $dir//deployments/* ; do 
  kubectl apply -f $deployment
done
