#!/bin/bash
dir=$1

for deployment in $dir/deployments/* ; do 
  kubectl delete -f $deployment
done

for service in $dir/services/* ; do 
  kubectl delete -f $service
done
